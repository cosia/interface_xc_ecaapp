package com.co.extreme.xc.ecaapp.ws.exception;

/**
 *
 * @author Cristian Z. Osia
 */
public class MyException extends Exception {
    
    private static final long serialVersionUID = -8684039289536492570L;
    public static final int E_DB = 0;
    public static final int E_CL = 1;
    public static final int E_NL = 2;
    private final int type;
    private final String msj;

    public MyException(int type, String msj) {
        this.type = type;
        this.msj = msj;
    }

    public int getType() {
        return type;
    }

    @Override
    public String getMessage() {
        return msj;
    }
    
}
