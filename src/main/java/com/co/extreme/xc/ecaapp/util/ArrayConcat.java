package com.co.extreme.xc.ecaapp.util;

import java.util.ArrayList;

/**
 *
 * @author Cristian Z. Osia
 */
public class ArrayConcat {
    
    private ArrayList data = new ArrayList();

    public ArrayList getData() {
        return data;
    }

    public void setData(ArrayList data) {
        this.data = data;
    }

    public void addData(ArrayList data) {
        if (data != null) {
            this.data.addAll(data);
        } else {
            data = new ArrayList();
            this.data.addAll(data);
        }
    }
    
}
