package com.co.extreme.xc.ecaapp.util;

/**
 *
 * @author Cristian Z. Osia
 */
public class Util {

    public static String POOL_NAME;
    // PLANTILLA DE FINCA
    public static final String PLANTILLA_FINCA = "Fincas";
    // PLANTILLA DE LOCALIDADES
    public static final String PLANTILLA_LOCAL = "Localidades";
    // PLANTILLA DE CT'S
    public static final String PLANTILLA_CTS_TRAFOS = "CT's_trafos";
    // PLANTILLA DE CT'S
    public static final String PLANTILLA_CTS = "CT's";
    // PLANTILLA DE PROVINCIAS
    public static final String PLANTILLA_PROV = "Provincias";
    // PLANTILLA DE TRAFOS-SUMINISTROS
    public static final String PLANTILLA_TRAFO = "Trafos-suministros";
    // PLANTILLA DE RED MT-BT
    public static final String PLANTILLA_RED = "Red MT-BT";

    public static final String COD_PLANTILLA_SIN_SELECCION = "00"; //Seleccionar";
    public static final String COD_PLANTILLA_FINCA = "01"; //Fincas";
    public static final String COD_PLANTILLA_LOCAL = "02"; //"Localidades";
    public static final String COD_PLANTILLA_CTS_TRAFOS = "03"; //"CT's_trafos";
    public static final String COD_PLANTILLA_CTS = "04"; //"CT's";
    public static final String COD_PLANTILLA_PROV = "05"; //"Provincias";
    public static final String COD_PLANTILLA_TRAFO = "06"; //"Trafos-suministros";
    public static final String COD_PLANTILLA_RED = "07"; //"Red MT-BT";
    
}
