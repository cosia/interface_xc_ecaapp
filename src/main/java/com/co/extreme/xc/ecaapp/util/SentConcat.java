package com.co.extreme.xc.ecaapp.util;

/**
 *
 * @author Cristian Z. Osia
 */
public class SentConcat {
    
    private String sentencia = "";

    public String getSentencia() {
        return sentencia;
    }

    public synchronized void addSentencia(String value) {
        sentencia += value;
    }

    public synchronized void setSentencia(String value) {
        sentencia = value;
    }
    
}
