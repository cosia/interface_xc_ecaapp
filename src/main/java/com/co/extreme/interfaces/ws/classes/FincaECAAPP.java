package com.co.extreme.interfaces.ws.classes;

/**
 *
 * @author Cristian Z. Osia
 */
public class FincaECAAPP {
    
    private String nisRad;
    private String nic;
    private String nif;
    private String cliente;
    private String direccion;
    private String cgvSum;
    private String codCalle;
    private String codLocal;
    private String nomLocal;
    private String codMunic;
    private String nomMunic;
    private String codDepto;
    private String nomDepto;
    private String codProv;
    private String nomProv;
    private String longitud;
    private String latitud;

    public String getNisRad() {
        return nisRad;
    }

    public void setNisRad(String nisRad) {
        this.nisRad = nisRad;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCgvSum() {
        return cgvSum;
    }

    public void setCgvSum(String cgvSum) {
        this.cgvSum = cgvSum;
    }

    public String getCodCalle() {
        return codCalle;
    }

    public void setCodCalle(String codCalle) {
        this.codCalle = codCalle;
    }

    public String getCodLocal() {
        return codLocal;
    }

    public void setCodLocal(String codLocal) {
        this.codLocal = codLocal;
    }

    public String getNomLocal() {
        return nomLocal;
    }

    public void setNomLocal(String nomLocal) {
        this.nomLocal = nomLocal;
    }

    public String getCodMunic() {
        return codMunic;
    }

    public void setCodMunic(String codMunic) {
        this.codMunic = codMunic;
    }

    public String getNomMunic() {
        return nomMunic;
    }

    public void setNomMunic(String nomMunic) {
        this.nomMunic = nomMunic;
    }

    public String getCodDepto() {
        return codDepto;
    }

    public void setCodDepto(String codDepto) {
        this.codDepto = codDepto;
    }

    public String getNomDepto() {
        return nomDepto;
    }

    public void setNomDepto(String nomDepto) {
        this.nomDepto = nomDepto;
    }

    public String getCodProv() {
        return codProv;
    }

    public void setCodProv(String codProv) {
        this.codProv = codProv;
    }

    public String getNomProv() {
        return nomProv;
    }

    public void setNomProv(String nomProv) {
        this.nomProv = nomProv;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof String) {
            String f = (String) obj;
            return (f.equals(nic));
        } else if (obj instanceof FincaECAAPP) {
            FincaECAAPP f = (FincaECAAPP) obj;
            return (f.nic.equals(nic));
        }
        return false;
    }
    
}
