package com.co.extreme.interfaces.ws.rest;

import com.google.gson.Gson;

/**
 *
 * @author Cristian Z. Osia
 */
public class AppRest {
    
    protected final Gson gson;

    public AppRest() {
        gson = new Gson();
    }
}
