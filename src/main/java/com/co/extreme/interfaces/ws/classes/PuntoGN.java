package com.co.extreme.interfaces.ws.classes;

/**
 *
 * @author Cristian Z. Osia
 */
public class PuntoGN {
    
    private String codInstalacion;
    private String codOrd;
    private String chipGN;
    private String malla;
    private String poliza;

    public PuntoGN() {
    }

    public String getCodInstalacion() {
        return codInstalacion;
    }

    public String getCodOrd() {
        return codOrd;
    }

    public String getChipGN() {
        return chipGN;
    }

    public String getMalla() {
        return malla;
    }

    public String getPoliza() {
        return poliza;
    }
    
}
