package com.co.extreme.interfaces.ws.rest;

import com.co.extreme.interfaces.ws.classes.CtsECAAPP;
import com.co.extreme.interfaces.ws.classes.CtsTrafoECAAPP;
import com.co.extreme.interfaces.ws.classes.FincaECAAPP;
import com.co.extreme.interfaces.ws.classes.LocalidadECAAPP;
import com.co.extreme.interfaces.ws.classes.Plantilla;
import com.co.extreme.interfaces.ws.classes.ProvinciaECAAPP;
import com.co.extreme.interfaces.ws.classes.PuntoGN;
import com.co.extreme.interfaces.ws.classes.RedECAAPP;
import com.co.extreme.interfaces.ws.classes.ResponseInterface;
import com.co.extreme.interfaces.ws.classes.TrafoECAAPP;
import com.co.extreme.interfaces.ws.facades.DatosECAAPPFacade;
import com.co.extreme.xc.ecaapp.util.Util;
import com.co.extreme.xc.ecaapp.ws.exception.MyException;
import com.extreme.commons.vo.http.MyHttpResponse;
import com.extreme.commons.vo.utils.CommonsUtils;
import com.extreme.db.myexception.DBException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 *
 * @author Cristian Z. Osia
 */
@Path("/ecaapp")
public class ECAAPPRest extends AppRest {
    
    @POST
    @Path("/iniciar")
    @Produces("application/json")
    public String iniciar(@FormParam("token") String token, @FormParam("plantilla") String plantilla) throws DBException {
        try {
            switch (plantilla) {
                case Util.PLANTILLA_CTS_TRAFOS:
                    new DatosECAAPPFacade().iniciarProcesoCts_Trafo();
                    break;
                case Util.PLANTILLA_CTS:
                    new DatosECAAPPFacade().iniciarProcesoCts();
                case Util.PLANTILLA_FINCA:
                    new DatosECAAPPFacade().iniciarProcesoFincas();
                    break;
                case Util.PLANTILLA_LOCAL:
                    new DatosECAAPPFacade().iniciarProcesoLocalidades();
                    break;
                case Util.PLANTILLA_PROV:
                    new DatosECAAPPFacade().iniciarProcesoProvincias();
                    break;
                case Util.PLANTILLA_RED:
                    new DatosECAAPPFacade().iniciarProcesoRedes();
                    break;
                case Util.PLANTILLA_TRAFO:
                    new DatosECAAPPFacade().iniciarProcesoTrafos();
                    break;
            }
            return "true";
        } catch (DBException ex) {
            System.out.println("ERROR: "+ex);
        }
        return "false";
    }
    
    @POST
    @Path("/limpiar")
    @Produces("application/json")
    public String limpiar(@FormParam("plantilla") String plantilla) throws DBException {
        Plantilla p = new Plantilla();
        Type type = new TypeToken<Plantilla>() {
        }.getType();
        final MyHttpResponse<Boolean> response = new MyHttpResponse();
        if (plantilla != null && !plantilla.isEmpty()) {
            p = gson.fromJson(plantilla, type);
        }        
        try {
            boolean estado = false;
            switch (p.getId()) {
                case Util.COD_PLANTILLA_CTS_TRAFOS:
                    estado = new DatosECAAPPFacade().deleteCtsTrafosTemp();
                    break;
                case Util.COD_PLANTILLA_CTS:
                    estado = new DatosECAAPPFacade().deleteCtsTemp();
                    break;
                case Util.COD_PLANTILLA_FINCA:
                    estado = new DatosECAAPPFacade().deleteFincasTemp();
                    break;
                case Util.COD_PLANTILLA_LOCAL:
                    estado = new DatosECAAPPFacade().deleteLocalidadesTemp();
                    break;
                case Util.COD_PLANTILLA_PROV:
                    estado = new DatosECAAPPFacade().deleteProvinciasTemp();
                    break;
                case Util.COD_PLANTILLA_RED:
                    estado = new DatosECAAPPFacade().deleteRedesTemp();
                    break;
                case Util.COD_PLANTILLA_TRAFO:
                    estado = new DatosECAAPPFacade().deleteTrafosTemp();
                    break;
            }
            final ArrayList<Boolean> datos = new ArrayList();
            datos.add(estado);
            if (estado) {
                response.setEst(CommonsUtils.EST_OK);
                response.seteDB(CommonsUtils.EST_OK);
                response.setMsj("Temporales borrados...");
                response.setDatos(datos);
            } else {
                response.setEst(CommonsUtils.EST_ERROR);
                response.seteDB(CommonsUtils.EST_OK);
                response.setMsj("Temporales no fueron borrados");
            }
        } catch (MyException e) {
            switch (e.getType()) {
                case MyException.E_DB:
                    response.setEst(CommonsUtils.EST_OK);
                    response.seteDB(CommonsUtils.EST_ERROR);
                    break;
                case MyException.E_CL:
                case MyException.E_NL:
                    response.setEst(CommonsUtils.EST_ERROR);
                    response.seteDB(CommonsUtils.EST_OK);
                    break;
            }
            response.setMsj(e.getMessage());
        }
        return gson.toJson(response);
    }

    @POST
    @Path("/datos")
    @Produces("application/json")
    public String polizas(@FormParam("data") String data,
        @FormParam("token") String token,@FormParam("plantilla") String plantilla) throws DBException {

        ResponseInterface response = null;
        if (data != null && !(data = data.trim()).isEmpty()) {
            try {
                switch (plantilla) {
                    case Util.PLANTILLA_CTS_TRAFOS:
                        Type type = new TypeToken<ArrayList<CtsTrafoECAAPP>>(){
                        }.getType();
                        ArrayList<CtsTrafoECAAPP> cts_Trafo = gson.fromJson(data, type);
                        response =  new DatosECAAPPFacade().procesarCtsTrafo(cts_Trafo);
                        break;
                    case Util.PLANTILLA_CTS:
                        Type type7 = new TypeToken<ArrayList<CtsECAAPP>>(){
                        }.getType();
                        ArrayList<CtsECAAPP> cts = gson.fromJson(data, type7);
                        response = new DatosECAAPPFacade().procesarCts(cts);
                        break;
                    case Util.PLANTILLA_FINCA:
                         Type type2 = new TypeToken<ArrayList<FincaECAAPP>>(){
                        }.getType();
                        ArrayList<FincaECAAPP> fincas = gson.fromJson(data, type2);
                        response =  new DatosECAAPPFacade().procesarFincas(fincas);
                        break;
                    case Util.PLANTILLA_LOCAL:
                        Type type3 = new TypeToken<ArrayList<LocalidadECAAPP>>(){
                        }.getType();
                        ArrayList<LocalidadECAAPP> localidades = gson.fromJson(data, type3);
                        response =  new DatosECAAPPFacade().procesarLocalidades(localidades);
                        break;
                    case Util.PLANTILLA_PROV:
                        Type type4 = new TypeToken<ArrayList<ProvinciaECAAPP>>(){
                        }.getType();
                        ArrayList<ProvinciaECAAPP> provincias = gson.fromJson(data, type4);
                        response =  new DatosECAAPPFacade().procesarProvincias(provincias);
                        break;
                    case Util.PLANTILLA_RED:
                        Type type5 = new TypeToken<ArrayList<RedECAAPP>>(){
                        }.getType();
                        ArrayList<RedECAAPP> redes = gson.fromJson(data, type5);
                        response =  new DatosECAAPPFacade().procesarRedes(redes);
                        break;
                    case Util.PLANTILLA_TRAFO:
                        Type type6 = new TypeToken<ArrayList<TrafoECAAPP>>(){
                        }.getType();
                        ArrayList<TrafoECAAPP> trafos = gson.fromJson(data, type6);
                        response =  new DatosECAAPPFacade().procesarTrafos(trafos);
                        break;
                }
               /* Type type = new TypeToken<ArrayList<PolizaGN>>() {
                }.getType();
                ArrayList<PolizaGN> polizas = gson.fromJson(data, type);
                response = new PolizasGNFacade().procesarPolizas(polizas);*/
            } catch (JsonSyntaxException ex) {
                response = new ResponseInterface("Formato de datos incorrecto");
            }
        } else {
            response = new ResponseInterface("No hay datos");
        }
        return gson.toJson(response);
    }
    
    @POST
    @Path("/maestros")
    @Produces("application/json")
    public String maestros(@FormParam("plantilla") String plantilla, @FormParam("data") String data) throws DBException {
        ResponseInterface response = null;
        Plantilla p = new Plantilla();
        Type typeP = new TypeToken<Plantilla>() {
        }.getType();
        if (plantilla != null && !plantilla.isEmpty()) {
            p = gson.fromJson(plantilla, typeP);
        } 
        if (data != null && !(data = data.trim()).isEmpty()) {
            try {
                switch (p.getId()) {
                    case Util.COD_PLANTILLA_CTS_TRAFOS:
                        Type type = new TypeToken<ArrayList<CtsTrafoECAAPP>>(){
                        }.getType();
                        ArrayList<CtsTrafoECAAPP> cts_Trafo = gson.fromJson(data, type);
                        response =  new DatosECAAPPFacade().procesarCtsTrafo(cts_Trafo);
                        break;
                    case Util.COD_PLANTILLA_CTS:
                        Type type7 = new TypeToken<ArrayList<CtsECAAPP>>(){
                        }.getType();
                        ArrayList<CtsECAAPP> cts = gson.fromJson(data, type7);
                        response = new DatosECAAPPFacade().procesarCtsT(cts);
                        break;
                    case Util.COD_PLANTILLA_FINCA:
                         Type type2 = new TypeToken<ArrayList<FincaECAAPP>>(){
                        }.getType();
                        ArrayList<FincaECAAPP> fincas = gson.fromJson(data, type2);
                        response =  new DatosECAAPPFacade().procesarFincasT(fincas);
                        break;
                    case Util.COD_PLANTILLA_LOCAL:
                        Type type3 = new TypeToken<ArrayList<LocalidadECAAPP>>(){
                        }.getType();
                        ArrayList<LocalidadECAAPP> localidades = gson.fromJson(data, type3);
                        response =  new DatosECAAPPFacade().procesarLocalidades(localidades);
                        break;
                    case Util.COD_PLANTILLA_PROV:
                        Type type4 = new TypeToken<ArrayList<ProvinciaECAAPP>>(){
                        }.getType();
                        ArrayList<ProvinciaECAAPP> provincias = gson.fromJson(data, type4);
                        response =  new DatosECAAPPFacade().procesarProvincias(provincias);
                        break;
                    case Util.COD_PLANTILLA_RED:
                        Type type5 = new TypeToken<ArrayList<RedECAAPP>>(){
                        }.getType();
                        ArrayList<RedECAAPP> redes = gson.fromJson(data, type5);
                        response =  new DatosECAAPPFacade().procesarRedes(redes);
                        break;
                    case Util.COD_PLANTILLA_TRAFO:
                        Type type6 = new TypeToken<ArrayList<TrafoECAAPP>>(){
                        }.getType();
                        ArrayList<TrafoECAAPP> trafos = gson.fromJson(data, type6);
                        response =  new DatosECAAPPFacade().procesarTrafos(trafos);
                        break;
                }
               /* Type type = new TypeToken<ArrayList<PolizaGN>>() {
                }.getType();
                ArrayList<PolizaGN> polizas = gson.fromJson(data, type);
                response = new PolizasGNFacade().procesarPolizas(polizas);*/
            } catch (JsonSyntaxException ex) {
                response = new ResponseInterface("Formato de datos incorrecto");
            }
        } else {
            response = new ResponseInterface("No hay datos");
        }
        return gson.toJson(response);
    }

    @POST
    @Path("/puntos")
    @Produces("application/json")
    public String puntos(@FormParam("data") String data,
        @FormParam("token") String token) throws DBException {

        ResponseInterface response;
        if (data != null && !(data = data.trim()).isEmpty()) {
            try {
                Type type = new TypeToken<ArrayList<PuntoGN>>() {
                }.getType();
                ArrayList<PuntoGN> puntos = gson.fromJson(data, type);
                response = new DatosECAAPPFacade().procesarPuntos(puntos);
            } catch (JsonSyntaxException ex) {
                response = new ResponseInterface("Formato de datos incorrecto");
            }
        } else {
            response = new ResponseInterface("No hay datos");
        }
        return gson.toJson(response);
    }

    @POST
    @Path("/finalizar")
    @Produces("application/json")
    public String finalizar(@FormParam("token") String token,@FormParam("plantilla") String plantilla) throws DBException {
        String respuesta="";
        try {
            switch (plantilla) {
                case Util.PLANTILLA_CTS_TRAFOS:
                    respuesta = new DatosECAAPPFacade().finalizarProcesoCtsTrafos();
                    break;
                case Util.PLANTILLA_CTS:
                    respuesta = new DatosECAAPPFacade().finalizarProcesoCts();
                    break;
                case Util.PLANTILLA_FINCA:
                    respuesta = new DatosECAAPPFacade().finalizarProcesoFincas();
                    break;
                case Util.PLANTILLA_LOCAL:
                    respuesta = new DatosECAAPPFacade().finalizarProcesoLocalidades();
                    break;
                case Util.PLANTILLA_PROV:
                    respuesta = new DatosECAAPPFacade().finalizarProcesoProvincias();
                    break;
                case Util.PLANTILLA_RED:
                    respuesta = new DatosECAAPPFacade().finalizarProcesoRedes();
                    break;
                case Util.PLANTILLA_TRAFO:
                    respuesta =new DatosECAAPPFacade().finalizarProcesoTrafos();
                    break;
            }
            return respuesta;
        } catch (DBException ex) {
            System.out.println(ex);
        }
        return "false";
    }    
    
    @POST
    @Path("/finalizarOp")
    @Produces("application/json")
    public String finalizar(@FormParam("plantilla") String plantilla) throws DBException {
        Plantilla p = new Plantilla();
        Type type = new TypeToken<Plantilla>() {
        }.getType();
        final MyHttpResponse<String> response = new MyHttpResponse();
        if (plantilla != null && !plantilla.isEmpty()) {
            p = gson.fromJson(plantilla, type);
        }        
        try {
            String respuesta = "";            
            switch (p.getId()) {
                case Util.COD_PLANTILLA_CTS_TRAFOS:
                    respuesta = new DatosECAAPPFacade().finalizarProcesoCtsTrafos();
                    break;
                case Util.COD_PLANTILLA_CTS:
                    respuesta = new DatosECAAPPFacade().finalizarProcesoCtsT();
                    break;
                case Util.COD_PLANTILLA_FINCA:
                    respuesta = new DatosECAAPPFacade().finalizarProcesoFincasT();
                    break;
                case Util.COD_PLANTILLA_LOCAL:
                    respuesta = new DatosECAAPPFacade().finalizarProcesoLocalidades();
                    break;
                case Util.COD_PLANTILLA_PROV:
                    respuesta = new DatosECAAPPFacade().finalizarProcesoProvincias();
                    break;
                case Util.COD_PLANTILLA_RED:
                    respuesta = new DatosECAAPPFacade().finalizarProcesoRedes();
                    break;
                case Util.COD_PLANTILLA_TRAFO:
                    respuesta =new DatosECAAPPFacade().finalizarProcesoTrafos();
                    break;
            }            
            final ArrayList<String> datos = new ArrayList();
            datos.add(respuesta);
            if (respuesta != null && !respuesta.trim().isEmpty()) {
                response.setEst(CommonsUtils.EST_OK);
                response.seteDB(CommonsUtils.EST_OK);
                response.setMsj("Finalizado...");
                response.setDatos(datos);
            } else {
                response.setEst(CommonsUtils.EST_ERROR);
                response.seteDB(CommonsUtils.EST_OK);
                response.setMsj("No se pudo finalizar");
            }
        } catch (MyException e) {
            switch (e.getType()) {
                case MyException.E_DB:
                    response.setEst(CommonsUtils.EST_OK);
                    response.seteDB(CommonsUtils.EST_ERROR);
                    break;
                case MyException.E_CL:
                case MyException.E_NL:
                    response.setEst(CommonsUtils.EST_ERROR);
                    response.seteDB(CommonsUtils.EST_OK);
                    break;
            }
            response.setMsj(e.getMessage());
        }
        return gson.toJson(response);
    }
    
}
