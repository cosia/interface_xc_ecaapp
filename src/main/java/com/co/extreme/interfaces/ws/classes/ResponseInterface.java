package com.co.extreme.interfaces.ws.classes;

import java.util.ArrayList;

/**
 *
 * @author Cristian Z. Osia
 */
public class ResponseInterface {
    
    private String msj;
    private final ArrayList<String> ok;
    private final ArrayList<String[]> errors;

    public ResponseInterface() {
        ok = new ArrayList();
        errors = new ArrayList();
    }

    public ResponseInterface(final String msj) {
        this();
        this.msj = msj;
    }

    public String getMsj() {
        return msj;
    }

    public ArrayList<String> getOk() {
        return ok;
    }

    public ArrayList<String[]> getErrors() {
        return errors;
    }

    public void addOk(String value) {
        ok.add(value);
    }

    public void addAllOk(ArrayList<String> value) {
        ok.addAll(value);
    }

    public void addError(String[] value) {
        errors.add(value);
    }
    
}
