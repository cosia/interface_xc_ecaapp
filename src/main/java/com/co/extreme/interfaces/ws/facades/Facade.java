package com.co.extreme.interfaces.ws.facades;

import com.co.extreme.interfaces.ws.classes.ResponseInterface;
import com.extreme.db.DBQuery;
import com.extreme.db.DBTransactions;
import com.extreme.db.myexception.DBException;
import java.util.ArrayList;
import java.util.Properties;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author Cristian Z. Osia
 */
class Facade {
    
    public static String DB_POOL;
    protected DBQuery db;
    protected final ResponseInterface response;
    protected final DBTransactions dbt;

    public Facade() throws DBException {
        if (DB_POOL == null) {
            try {
                Context context = new InitialContext();
                String mname = (String) context.lookup("java:app/AppName");
                Properties prop = (Properties) context.lookup("prop/serverpools");
                DB_POOL = prop.getProperty(mname);
            } catch (NamingException ex) {
            }
            DBTransactions.poolName = DB_POOL;
        }
        response = new ResponseInterface();
        db = new DBQuery(DB_POOL);
        dbt = DBTransactions.getInstance();
    }
    
    protected <T> ArrayList<T> selectQuery(String sql, Class<T> class_,
        Object... parameters) throws DBException {
        return db.selectQuery(sql, class_, parameters);
    }

    protected int updateQuery(String sql, Object... parameters) throws DBException {
        return db.updateQuery(sql, parameters);
    }

    protected void close() {
        if (db != null) {
            db.close();
        }
    }
    
}
