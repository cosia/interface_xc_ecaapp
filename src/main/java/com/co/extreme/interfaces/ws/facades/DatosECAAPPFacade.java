package com.co.extreme.interfaces.ws.facades;

import com.co.extreme.interfaces.ws.classes.CtsECAAPP;
import com.co.extreme.interfaces.ws.classes.CtsTrafoECAAPP;
import com.co.extreme.interfaces.ws.classes.FincaECAAPP;
import com.co.extreme.interfaces.ws.classes.LocalidadECAAPP;
import com.co.extreme.interfaces.ws.classes.PolizaGN;
import com.co.extreme.interfaces.ws.classes.ProvinciaECAAPP;
import com.co.extreme.interfaces.ws.classes.PuntoGN;
import com.co.extreme.interfaces.ws.classes.RedECAAPP;
import com.co.extreme.interfaces.ws.classes.ResponseInterface;
import com.co.extreme.interfaces.ws.classes.TrafoECAAPP;
import com.co.extreme.xc.ecaapp.util.ArrayConcat;
import com.co.extreme.xc.ecaapp.util.SentConcat;
import com.co.extreme.xc.ecaapp.ws.exception.MyException;
import com.extreme.db.DBQuery;
import com.extreme.db.DBTransactions;
import com.extreme.db.classes.RowIndex;
import com.extreme.db.myexception.DBException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Cristian Z. Osia
 */
public class DatosECAAPPFacade extends Facade {

    public DatosECAAPPFacade() throws DBException {
        super();
    }

    public void iniciarProceso() throws DBException {
        try {
            //db.updateQuery("TRUNCATE polizas_temp");
            db.updateQuery("DELETE FROM \"polizas_temp\"");
        } finally {
            close();
        }
    }

    public void iniciarProcesoCts_Trafo() throws DBException {
        try {
            //db.updateQuery("TRUNCATE polizas_temp");
            db.updateQuery("DELETE FROM \"CT_trafo_temp\"");
        } finally {
            close();
        }
    }

    public void iniciarProcesoCts() throws DBException {
        try {
            //db.updateQuery("TRUNCATE polizas_temp");
            db.updateQuery("DELETE FROM \"CT_temp\"");
        } finally {
            close();
        }
    }

    public void iniciarProcesoFincas() throws DBException {
        try {
            //db.updateQuery("TRUNCATE polizas_temp");
            db.updateQuery("DELETE FROM \"Fincas_temp\"");
        } finally {
            close();
        }
    }
    
    public boolean deleteCtsTrafosTemp() throws MyException {
        try {
            db.updateQuery("DELETE FROM \"Fincas_temp\"");
        } finally {
            close();
            return true;
        }
    }
    
    public boolean deleteCtsTemp() throws MyException {
        try {
            db.updateQuery("DELETE FROM \"Fincas_temp\"");
        } finally {
            close();
            return true;
        }
    }
    
    public boolean deleteFincasTemp() throws MyException {
        try {
            db.updateQuery("DELETE FROM \"Fincas_temp\"");
        } finally {
            close();
            return true;
        }
    }
    
    public boolean deleteLocalidadesTemp() throws MyException {
        try {
            db.updateQuery("DELETE FROM \"Fincas_temp\"");
        } finally {
            close();
            return true;
        }
    }
    
    public boolean deleteProvinciasTemp() throws MyException {
        try {
            db.updateQuery("DELETE FROM \"Fincas_temp\"");
        } finally {
            close();
            return true;
        }
    }

    public boolean deleteRedesTemp() throws MyException {
        try {
            db.updateQuery("DELETE FROM \"Fincas_temp\"");
        } finally {
            close();
            return true;
        }
    }
    
    public boolean deleteTrafosTemp() throws MyException {
        try {
            db.updateQuery("DELETE FROM \"Fincas_temp\"");
        } finally {
            close();
            return true;
        }
    }

    public void iniciarProcesoLocalidades() throws DBException {
        try {
            //db.updateQuery("TRUNCATE polizas_temp");
            db.updateQuery("DELETE FROM \"Localidades_temp\"");
        } finally {
            close();
        }
    }

    public void iniciarProcesoProvincias() throws DBException {
        try {
            //db.updateQuery("TRUNCATE polizas_temp");
            db.updateQuery("DELETE FROM \"Provincias_temp\"");
        } finally {
            close();
        }
    }

    public void iniciarProcesoRedes() throws DBException {
        try {
            //db.updateQuery("TRUNCATE polizas_temp");
            db.updateQuery("DELETE FROM \"RED-MT-BT_temp\"");
        } finally {
            close();
        }
    }

    public void iniciarProcesoTrafos() throws DBException {
        try {
            //db.updateQuery("TRUNCATE polizas_temp");
            db.updateQuery("DELETE FROM \"TrafoSuministros_temp\"");
        } finally {
            close();
        }
    }

    public ResponseInterface procesarPolizas(ArrayList<PolizaGN> polizas) {
        if (polizas != null && !polizas.isEmpty()) {
            String sql = "INSERT INTO polizas_temp (cliente, municipio, nombre, direccion, "
                    + "telefono, medidor, marcamed, tipomed, estrato, tipocliente, "
                    + "situacioncliente, tarifa, fechacontratacion, fechapuestaservicio, "
                    + "categoriaccial, latitud, longitud, malla) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)";
            try {
                polizas.stream().forEach((polizaGN) -> {
                    String poliza = polizaGN.getPoliza();
                    try {
                        if (db.updateQuery(sql, poliza, polizaGN.getMunicipio(),
                                polizaGN.getNombre(), polizaGN.getDireccion(),
                                polizaGN.getTelefono(), polizaGN.getMedidor(),
                                polizaGN.getMedidorMarca(), polizaGN.getMedidorTipo(),
                                polizaGN.getEstrato(), polizaGN.getClienteTipo(),
                                polizaGN.getClienteSituacion(), polizaGN.getTarifa(),
                                polizaGN.getFechaContratacion(), polizaGN.getFechaPuestaServicio(),
                                polizaGN.getCategoria(), polizaGN.getLatitud(), polizaGN.getLongitud(), polizaGN.getMalla()) > 0) {
                            response.addOk(poliza);
                        } else {
                            response.addError(new String[]{poliza, "ERROR INGRESANDO POLIZA"});
                        }
                    } catch (DBException ex) {
                        response.addError(new String[]{poliza, ex.getDBMessage()});
                        db.close();
                        try {
                            db = new DBQuery(DB_POOL, false);
                        } catch (Exception e) {
                            response.addError(new String[]{poliza, ex.getDBMessage()});
                        }
                    }
                });
                if (db != null) {
                    db.commit();
                    db.close();
                }
            } catch (DBException ex) {
                return new ResponseInterface(ex.getDBMessage());
            } finally {
                close();
            }
        }
        return response;
    }

    public ResponseInterface procesarCtsTrafo(ArrayList<CtsTrafoECAAPP> cts) {
        if (cts != null && !cts.isEmpty()) {
            String sql = "INSERT INTO \"CT_trafo_temp\" ("
                    + "codigo_trafo, matricula_trafo, codigo_ct, matricula_ct, potencia_nominal, "
                    + " codigo_salmt, matricula_salmt, nombre_salmt, nro_centro, empresa, "
                    + " nro_cmd, zona, nro_mesa, delegacion, propiedad, localizacion, "
                    + " latitud, longitud) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            cts.stream().forEach((CtsTrafoECAAPP) -> {
                String cts_trafo = CtsTrafoECAAPP.getCodCt();
                try {
                    if (db.updateQuery(sql, CtsTrafoECAAPP.getCodTrafo(), CtsTrafoECAAPP.getMatTrafo(), cts_trafo,
                            CtsTrafoECAAPP.getMatCt(), CtsTrafoECAAPP.getPotNominal(), CtsTrafoECAAPP.getCodSalmt(),
                            CtsTrafoECAAPP.getMatSalmt(), CtsTrafoECAAPP.getNombreSalmt(),
                            CtsTrafoECAAPP.getNroCentro(), CtsTrafoECAAPP.getEmpresa(),
                            CtsTrafoECAAPP.getNroCmd(), CtsTrafoECAAPP.getZona(),
                            CtsTrafoECAAPP.getNroMesa(), CtsTrafoECAAPP.getDelegacion(),
                            CtsTrafoECAAPP.getPropiedad(), CtsTrafoECAAPP.getLocalizacion(),
                            CtsTrafoECAAPP.getLatitud(), CtsTrafoECAAPP.getLongitud()) > 0) {
                        response.addOk(cts_trafo);
                    } else {
                        response.addError(new String[]{cts_trafo, "ERROR INGRESANDO CT_TRAFO"});
                    }
                } catch (DBException ex) {
                    response.addError(new String[]{cts_trafo, ex.getDBMessage()});
                    try {
                        db.commit();
                    } catch (DBException ex1) {
                        Logger.getLogger(DatosECAAPPFacade.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                    //db.close();
                    try {
                        db = new DBQuery(DB_POOL, false);
                    } catch (DBException e) {
                    }
                    //response.addError(new String[]{finca, ex.getDBMessage()});

                }
            });
            if (db != null) {
                try {
                    db.commit();
                } catch (DBException ex1) {
                    Logger.getLogger(DatosECAAPPFacade.class.getName()).log(Level.SEVERE, null, ex1);
                } finally {
                    db.close();
                }

                db = null;
            }
        }
        return response;
    }

    public ResponseInterface procesarCts(ArrayList<CtsECAAPP> cts) {
        if (cts != null && !cts.isEmpty()) {
            String sql = "INSERT INTO \"CT_temp\"("
                    + "cod_ct, mat_ct, potencia_instalada, cod_sm, mat_sm, nom_sm, nro_centro, "
                    + "empresa, nro_cmd, zona_tr, nro_mesa, nom_delega, propietario, "
                    + "localizacion, longitud, latitud) VALUES (?, ?, ?, ?, ?, ?, ?, "
                    + "?, ?, ?, ?, ?, ?, ?, ?, ?)";
            cts.stream().forEach((CtsECAAPP) -> {
                String ct = CtsECAAPP.getCod_ct();
                try {
                    if (db.updateQuery(sql, ct, CtsECAAPP.getMat_ct(), CtsECAAPP.getPotencia_instalada(),
                            CtsECAAPP.getCod_sm(), CtsECAAPP.getMat_sm(), CtsECAAPP.getNom_sm(),
                            CtsECAAPP.getNro_centro(), CtsECAAPP.getEmpresa(),
                            CtsECAAPP.getNro_cmd(), CtsECAAPP.getZona_tr(),
                            CtsECAAPP.getNro_mesa(), CtsECAAPP.getNom_delega(),
                            CtsECAAPP.getPropietario(), CtsECAAPP.getLocalizacion(),
                            CtsECAAPP.getLongitud(), CtsECAAPP.getLatitud()) > 0) {
                        response.addOk(ct);
                    } else {
                        response.addError(new String[]{ct, "ERROR INGRESANDO CT"});
                    }
                } catch (DBException ex) {
                    response.addError(new String[]{ct, ex.getDBMessage()});
                    try {
                        db.commit();
                    } catch (DBException ex1) {
                        Logger.getLogger(DatosECAAPPFacade.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                    //db.close();
                    try {
                        db = new DBQuery(DB_POOL, false);
                    } catch (DBException e) {
                    }
                    //response.addError(new String[]{finca, ex.getDBMessage()});

                }
            });
            if (db != null) {
                try {
                    db.commit();
                } catch (DBException ex1) {
                    Logger.getLogger(DatosECAAPPFacade.class.getName()).log(Level.SEVERE, null, ex1);
                } finally {
                    db.close();
                }

                db = null;
            }
        }
        return response;
    }
    
    public ArrayList<CtsECAAPP> getCtsInserts(ArrayList<CtsECAAPP> cts) throws DBException {
        ArrayList<CtsECAAPP> ctsCod = new ArrayList<>();
        if (cts != null && !cts.isEmpty()) {
            SentConcat sentencia = new SentConcat();
            cts.stream().parallel().forEach(ct -> {
                sentencia.addSentencia("('" + ct.getCod_ct() + "','" + ct.getMat_ct()
                        + "','" + ct.getPotencia_instalada() + "','" + ct.getCod_sm() + "','" + ct.getMat_sm()
                        + "','" + ct.getNom_sm() + "','" + ct.getNro_centro() + "','" + ct.getEmpresa()
                        + "','" + ct.getNro_cmd() + "','" + ct.getZona_tr() + "','" + ct.getNro_mesa()
                        + "','" + ct.getNom_delega() + "','" + ct.getPropietario() + "','" + ct.getLocalizacion()
                        + "','" + ct.getLongitud() + "','" + ct.getLatitud() + "'),");
            });
            sentencia.setSentencia(sentencia.getSentencia().substring(0, sentencia.getSentencia().length() - 1));
            ctsCod = getCtsInserts(sentencia.getSentencia());
        }
        return ctsCod;
    }

    public ArrayList<FincaECAAPP> getFincasInserts(ArrayList<FincaECAAPP> fincas) throws DBException {
        ArrayList<FincaECAAPP> fincasNic = new ArrayList<>();
        if (fincas != null && !fincas.isEmpty()) {
            SentConcat sentencia = new SentConcat();
            fincas.stream().parallel().forEach(fin -> {
                sentencia.addSentencia("('" + fin.getNisRad() + "','" + fin.getNif()
                        + "','" + fin.getCliente() + "','" + fin.getDireccion() + "','" + fin.getCgvSum()
                        + "','" + fin.getCodCalle() + "','" + fin.getCodLocal() + "','" + fin.getNomLocal()
                        + "','" + fin.getCodMunic() + "','" + fin.getNomMunic() + "','" + fin.getCodDepto()
                        + "','" + fin.getNomDepto() + "','" + fin.getCodProv() + "','" + fin.getNomProv()
                        + "','" + fin.getLatitud() + "','" + fin.getLongitud() + "','" + fin.getNic() + "'),");
            });
            sentencia.setSentencia(sentencia.getSentencia().substring(0, sentencia.getSentencia().length() - 1));
            fincasNic = getFincasInserts(sentencia.getSentencia());
        }
        return fincasNic;
    }
    
    public ArrayList<CtsECAAPP> getCtsByCod(ArrayList<CtsECAAPP> cts) throws DBException {
        ArrayList<CtsECAAPP> ctsCod = new ArrayList<CtsECAAPP>();
        if (cts != null && !cts.isEmpty()) {
            SentConcat sentencia = new SentConcat();
            cts.stream().parallel().forEach(ct -> {
                sentencia.addSentencia("'" + ct.getCod_ct() + "',");
            });
            sentencia.setSentencia(sentencia.getSentencia().substring(0, sentencia.getSentencia().length() - 1));
            ctsCod = getCtsByCod(sentencia.getSentencia());
        }
        return ctsCod;
    }

    public ArrayList<FincaECAAPP> getFincasByNic(ArrayList<FincaECAAPP> fincas) throws DBException {
        ArrayList<FincaECAAPP> fincasNic = new ArrayList<FincaECAAPP>();
        if (fincas != null && !fincas.isEmpty()) {
            SentConcat sentencia = new SentConcat();
            fincas.stream().parallel().forEach(fin -> {
                sentencia.addSentencia("'" + fin.getNic() + "',");
            });
            sentencia.setSentencia(sentencia.getSentencia().substring(0, sentencia.getSentencia().length() - 1));
            fincasNic = getFincasByNic(sentencia.getSentencia());
        }
        return fincasNic;
    }
    
    public ArrayList<CtsECAAPP> getCtsInserts(String data) throws DBException {
        ArrayList<CtsECAAPP> cts = new ArrayList<>();
        DBQuery dbq = new DBQuery(DB_POOL, false);
        try {
            String sql = " INSERT INTO \"Fincas_temp\" "
                    + "(nis_rad, nif, cliente, direccion, cgv_sum, cod_calle, cod_local,"
                    + "nom_local, cod_munic, nom_munic, cod_depto, nom_depto, cod_prov,"
                    + "nom_prov, latitud, longitud, nic)"
                    + " VALUES " + data + " "
                    + "ON CONFLICT (nic) DO NOTHING "
                    + "RETURNING nis_rad as nisRad, "
                    + "nif, cliente, direccion, cgv_sum, cod_calle, cod_local, "
                    + "nom_local, cod_munic, nom_munic, cod_depto, nom_depto, cod_prov, "
                    + "nom_prov, latitud, longitud, nic ";
            cts = dbq.selectQuery(sql, CtsECAAPP.class);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbq.close();
        }
        return cts;
    }

    public ArrayList<FincaECAAPP> getFincasInserts(String data) throws DBException {
        ArrayList<FincaECAAPP> fincas = new ArrayList<>();
        DBQuery dbq = new DBQuery(DB_POOL, false);
        try {
            String sql = " INSERT INTO \"Fincas_temp\" "
                    + "(nis_rad, nif, cliente, direccion, cgv_sum, cod_calle, cod_local,"
                    + "nom_local, cod_munic, nom_munic, cod_depto, nom_depto, cod_prov,"
                    + "nom_prov, latitud, longitud, nic)"
                    + " VALUES " + data + " "
                    + "ON CONFLICT (nic) DO NOTHING "
                    + "RETURNING nis_rad as nisRad, "
                    + "nif, cliente, direccion, cgv_sum, cod_calle, cod_local, "
                    + "nom_local, cod_munic, nom_munic, cod_depto, nom_depto, cod_prov, "
                    + "nom_prov, latitud, longitud, nic ";
            fincas = dbq.selectQuery(sql, FincaECAAPP.class);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbq.close();
        }
        return fincas;
    }

    public ArrayList<CtsECAAPP> getCtsByCod(String data) throws DBException {
        ArrayList<CtsECAAPP> fincas = new ArrayList<CtsECAAPP>();
        DBQuery dbq = new DBQuery(DB_POOL);
        try {
            String sql = " SELECT * FROM \"Fincas_temp\" WHERE nic IN (" + data + ")";
            fincas = dbq.selectQuery(sql, CtsECAAPP.class);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbq.close();
        }
        return fincas;
    }
    
    public ArrayList<FincaECAAPP> getFincasByNic(String data) throws DBException {
        ArrayList<FincaECAAPP> fincas = new ArrayList<FincaECAAPP>();
        DBQuery dbq = new DBQuery(DB_POOL);
        try {
            String sql = " SELECT * FROM \"Fincas_temp\" WHERE nic IN (" + data + ")";
            fincas = dbq.selectQuery(sql, FincaECAAPP.class);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbq.close();
        }
        return fincas;
    }

    public ResponseInterface procesarFincasT(ArrayList<FincaECAAPP> fincas) throws DBException {
        if (fincas != null && !fincas.isEmpty()) {
            ArrayList<FincaECAAPP> fincasNic = getFincasByNic(fincas);
            if (fincasNic != null && !fincasNic.isEmpty()) {
                fincasNic.stream().forEach(fin -> {
                    response.addError(new String[]{fin.getNisRad(), "ERROR INGRESANDO FINCA: NIC REPETIDO"});
                });
                fincas.removeAll(fincasNic);
            }
            ArrayList<FincaECAAPP> fincasInserts = getFincasInserts(fincas);
            if (fincasInserts != null && !fincasInserts.isEmpty()) {
                fincasInserts.stream().forEach(fin -> {
                    response.addOk(fin.getNisRad());
                });
                fincas.removeAll(fincasInserts);
            }
            fincas.stream().forEach(fin -> {
                response.addError(new String[]{fin.getNisRad(), "ERROR INGRESANDO FINCA: NIC REPETIDO"});
            });
        }
        return response;
    }
    
    public ResponseInterface procesarCtsT(ArrayList<CtsECAAPP> cts) throws DBException {
        if (cts != null && !cts.isEmpty()) {
            ArrayList<CtsECAAPP> ctsCod = getCtsByCod(cts);
            if (ctsCod != null && !ctsCod.isEmpty()) {
                ctsCod.stream().forEach(ct -> {
                    response.addError(new String[]{ct.getCod_ct(), "ERROR INGRESANDO FINCA: COD REPETIDO"});
                });
                cts.removeAll(ctsCod);
            }
            ArrayList<CtsECAAPP> ctsInserts = getCtsInserts(cts);
            if (ctsInserts != null && !ctsInserts.isEmpty()) {
                ctsInserts.stream().forEach(ct -> {
                    response.addOk(ct.getCod_ct());
                });
                cts.removeAll(ctsInserts);
            }
            cts.stream().forEach(ct -> {
                response.addError(new String[]{ct.getCod_ct(), "ERROR INGRESANDO FINCA: COD REPETIDO"});
            });
        }
        return response;
    }
    
    public ResponseInterface procesarFincasTemp(ArrayList<FincaECAAPP> fincas) {
        if (fincas != null && !fincas.isEmpty()) {
            String sql = " INSERT INTO \"Fincas_temp\" "
                        + "(nis_rad, nif, cliente, direccion, cgv_sum, cod_calle, cod_local,"
                        + "nom_local, cod_munic, nom_munic, cod_depto, nom_depto, cod_prov,"
                        + "nom_prov, latitud, longitud, nic)"
                        + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            try {
                fincas.stream().forEach((fincaECAAPP) -> {
                    String finca = fincaECAAPP.getNisRad();
                    try {
                        if (db.updateQuery(sql, fincaECAAPP.getNisRad(), fincaECAAPP.getNif(), fincaECAAPP.getCliente(),
                                fincaECAAPP.getDireccion(), fincaECAAPP.getCgvSum(), fincaECAAPP.getCodCalle(), fincaECAAPP.getCodLocal(),
                                fincaECAAPP.getNomLocal(), fincaECAAPP.getCodMunic(), fincaECAAPP.getNomMunic(), fincaECAAPP.getCodDepto(),
                                fincaECAAPP.getNomDepto(), fincaECAAPP.getCodProv(), fincaECAAPP.getNomProv(),
                                fincaECAAPP.getLatitud(), fincaECAAPP.getLongitud(), fincaECAAPP.getNic()) > 0) {
                            response.addOk(finca);
                        } else {
                            response.addError(new String[]{finca, "ERROR INGRESANDO POLIZA"});
                        }
                    } catch (DBException ex) {
                        response.addError(new String[]{finca, ex.getDBMessage()});
                    }
                });
                db.commit();
            } catch (DBException ex) {
                return new ResponseInterface(ex.getDBMessage());
            } finally {
                close();
            }
        }
        return response;
    }

    public ResponseInterface procesarFincas(ArrayList<FincaECAAPP> fincas) throws DBException {

        if (fincas != null && !fincas.isEmpty()) {

            String sql = "INSERT INTO \"Fincas_temp\"("
                    + "nis_rad, nif, cliente, direccion, cgv_sum, cod_calle, cod_local,"
                    + " nom_local, cod_munic, nom_munic, cod_depto, nom_depto, cod_prov,"
                    + " nom_prov, latitud, longitud, nic)"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            String sql2 = "SELECT COUNT(*) AS index "
                    + "FROM \"Fincas_temp\" "
                    + "WHERE nic = ?";

            String sql3 = "UPDATE \"Fincas_temp\" "
                    + "SET nis_rad=?, nif=?, cliente=?, direccion=?, cgv_sum=?, cod_calle=?, "
                    + "cod_local=?, nom_local=?, cod_munic=?, nom_munic=?, cod_depto=?, "
                    + "nom_depto=?, cod_prov=?, nom_prov=?, latitud=?, longitud=? "
                    + "WHERE nic=?";
            fincas.stream().forEach((FincaECAAPP) -> {
                String finca = FincaECAAPP.getNisRad();
                try {
                    RowIndex cNic = db.getUnique(sql2, RowIndex.class, FincaECAAPP.getNic());
                    if (cNic.getIntIndex() == 0) {
                        if (db.updateQuery(sql3, FincaECAAPP.getNisRad(), FincaECAAPP.getNif(), FincaECAAPP.getCliente(),
                                FincaECAAPP.getDireccion(), FincaECAAPP.getCgvSum(),
                                FincaECAAPP.getCodCalle(), FincaECAAPP.getCodLocal(), FincaECAAPP.getNomLocal(),
                                FincaECAAPP.getCodMunic(), FincaECAAPP.getNomMunic(), FincaECAAPP.getCodDepto(),
                                FincaECAAPP.getNomDepto(), FincaECAAPP.getCodProv(), FincaECAAPP.getNomProv(),
                                FincaECAAPP.getLatitud(), FincaECAAPP.getLongitud(), FincaECAAPP.getNic()) == 0) {

                            if (db.updateQuery(sql, finca, FincaECAAPP.getNif(), FincaECAAPP.getCliente(),
                                    FincaECAAPP.getDireccion(), FincaECAAPP.getCgvSum(), FincaECAAPP.getCodCalle(),
                                    FincaECAAPP.getCodLocal(), FincaECAAPP.getNomLocal(),
                                    FincaECAAPP.getCodMunic(), FincaECAAPP.getNomMunic(),
                                    FincaECAAPP.getCodDepto(), FincaECAAPP.getNomDepto(),
                                    FincaECAAPP.getCodProv(), FincaECAAPP.getNomProv(),
                                    FincaECAAPP.getLatitud(), FincaECAAPP.getLongitud(),
                                    FincaECAAPP.getNic()) > 0) {
                                response.addOk(finca);
                            } else {
                                response.addError(new String[]{finca, "ERROR INGRESANDO FINCA"});
                            }
                        } else {
                            response.addOk(finca);
                        }

                    } else {
                        response.addError(new String[]{finca, "ERROR INGRESANDO FINCA: NIC REPETIDO"});
                    }
                } catch (DBException ex) {
                    response.addError(new String[]{finca, ex.getDBMessage()});
                    try {
                        db.commit();
                    } catch (DBException ex1) {
                        Logger.getLogger(DatosECAAPPFacade.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                    //db.close();
                    try {
                        db = new DBQuery(DB_POOL, false);
                    } catch (DBException e) {
                    }
                    //response.addError(new String[]{finca, ex.getDBMessage()});

                }
            });

            if (db != null) {
                try {
                    db.commit();
                } catch (DBException ex1) {
                    Logger.getLogger(DatosECAAPPFacade.class.getName()).log(Level.SEVERE, null, ex1);
                } finally {
                    db.close();
                }

                db = null;
            }
        }
        return response;
    }

    public ResponseInterface procesarLocalidades(ArrayList<LocalidadECAAPP> localidades) throws DBException {
        if (localidades != null && !localidades.isEmpty()) {

            String sql = "INSERT INTO \"Localidades_temp\"("
                    + "cod_local, nom_local, cod_munic, cod_depto, cod_prov)"
                    + " VALUES (?, ?, ?, ?, ?);";
            localidades.stream().forEach((LocalidadECAAPP) -> {
                String localidad = LocalidadECAAPP.getCodLocal();
                try {

                    if (db == null) {
                        db = new DBQuery(DB_POOL, false);
                    }

                    if (db.updateQuery(sql, localidad, LocalidadECAAPP.getNomLocal(), LocalidadECAAPP.getCodMunic(),
                            LocalidadECAAPP.getCodDepto(), LocalidadECAAPP.getCodProv()) > 0) {

                        response.addOk(localidad);
                    } else {
                        response.addError(new String[]{localidad, "ERROR INGRESANDO LOCALIDAD"});
                    }
                } catch (DBException ex) {
                    response.addError(new String[]{localidad, ex.getDBMessage()});
                    try {
                        db.commit();
                    } catch (DBException ex1) {
                        Logger.getLogger(DatosECAAPPFacade.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                    //db.close();
                    try {
                        db = new DBQuery(DB_POOL, false);
                    } catch (DBException e) {
                    }
                    //response.addError(new String[]{finca, ex.getDBMessage()});

                }
            });
            if (db != null) {
                try {
                    db.commit();
                } catch (DBException ex1) {
                    Logger.getLogger(DatosECAAPPFacade.class.getName()).log(Level.SEVERE, null, ex1);
                } finally {
                    db.close();
                }

                db = null;
            }
        }
        return response;
    }

    public ResponseInterface procesarProvincias(ArrayList<ProvinciaECAAPP> provincias) {
        if (provincias != null && !provincias.isEmpty()) {

            String sql = "INSERT INTO \"Provincias_temp\"("
                    + "cod_prov, nom_prov) VALUES (?, ?);";
            provincias.stream().forEach((ProvinciaECAAPP) -> {
                String provincia = ProvinciaECAAPP.getCodProv();
                try {
                    if (db.updateQuery(sql, provincia, ProvinciaECAAPP.getNomProv()) > 0) {
                        response.addOk(provincia);
                    } else {
                        response.addError(new String[]{provincia, "ERROR INGRESANDO PROVINCIA"});
                    }
                } catch (DBException ex) {
                    response.addError(new String[]{provincia, ex.getDBMessage()});
                    try {
                        db.commit();
                    } catch (DBException ex1) {
                        Logger.getLogger(DatosECAAPPFacade.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                    //db.close();
                    try {
                        db = new DBQuery(DB_POOL, false);
                    } catch (DBException e) {
                    }
                    //response.addError(new String[]{finca, ex.getDBMessage()});

                }
            });
            if (db != null) {
                try {
                    db.commit();
                } catch (DBException ex1) {
                    Logger.getLogger(DatosECAAPPFacade.class.getName()).log(Level.SEVERE, null, ex1);
                } finally {
                    db.close();
                }

                db = null;
            }
        }
        return response;
    }

    public ResponseInterface procesarRedes(ArrayList<RedECAAPP> redes) {
        if (redes != null && !redes.isEmpty()) {

            String sql = "INSERT INTO \"RED-MT-BT_temp\"("
                    + "zona_sub, sector_sub, cor_sub, codigo_subestacion, nombre_subestacion,"
                    + " codigo_circuito, nombre_circuito, codigo_ct, nombre_ct, placa_blanca,"
                    + " codigo_trafo, nombre_trafo, placa_amarilla)"
                    + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            redes.stream().forEach((RedECAAPP) -> {
                String red = RedECAAPP.getCodCt();
                try {
                    if (db.updateQuery(sql, RedECAAPP.getZonaSub(), RedECAAPP.getSectorSub(),
                            RedECAAPP.getCorSub(), RedECAAPP.getCodSub(), RedECAAPP.getNomSub(),
                            RedECAAPP.getCodCircuito(), RedECAAPP.getNomCircuito(), red,
                            RedECAAPP.getNomCt(), RedECAAPP.getPlacaBlanca(), RedECAAPP.getCodTrafo(),
                            RedECAAPP.getNomTrafo(), RedECAAPP.getPlacaAmarilla()) > 0) {
                        response.addOk(red);
                    } else {
                        response.addError(new String[]{red, "ERROR INGRESANDO RED"});
                    }
                } catch (DBException ex) {
                    response.addError(new String[]{red, ex.getDBMessage()});
                    try {
                        db.commit();
                    } catch (DBException ex1) {
                        Logger.getLogger(DatosECAAPPFacade.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                    //db.close();
                    try {
                        db = new DBQuery(DB_POOL, false);
                    } catch (DBException e) {
                    }
                    //response.addError(new String[]{finca, ex.getDBMessage()});

                }
            });
            if (db != null) {
                try {
                    db.commit();
                } catch (DBException ex1) {
                    Logger.getLogger(DatosECAAPPFacade.class.getName()).log(Level.SEVERE, null, ex1);
                } finally {
                    db.close();
                }

                db = null;
            }
        }
        return response;
    }

    public ResponseInterface procesarTrafos(ArrayList<TrafoECAAPP> trafos) {
        if (trafos != null && !trafos.isEmpty()) {

            String sql = "INSERT INTO \"TrafoSuministros_temp\"("
                    + "cod_trafo, matricula, nis_rad, nif, f_actual, h_actual, cod_tar,"
                    + " tip_serv) VALUES (?, ?, ?, ?,to_timestamp(?, 'yyyy-mm-dd hh24:mi') ,"
                    + " to_timestamp(?, 'yyyy-mm-dd hh24:mi'), ?, ?)";
            trafos.stream().forEach((TrafoECAAPP) -> {
                String trafo = TrafoECAAPP.getCodTrafo();
                String nisRad = TrafoECAAPP.getNisRad();
                try {
                    if (db.updateQuery(sql, trafo, TrafoECAAPP.getMatricula(),
                            nisRad, TrafoECAAPP.getNif(), TrafoECAAPP.getFechaActual(),
                            TrafoECAAPP.getHoraActual(), TrafoECAAPP.getCodTar(), TrafoECAAPP.getTipServ()) > 0) {
                        response.addOk(trafo);
                    } else {
                        response.addError(new String[]{trafo, "ERROR INGRESANDO TRAFO"});
                    }
                } catch (DBException ex) {
                    response.addError(new String[]{trafo, ex.getDBMessage()});
                    try {
                        db.commit();
                    } catch (DBException ex1) {
                        Logger.getLogger(DatosECAAPPFacade.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                    //db.close();
                    try {
                        db = new DBQuery(DB_POOL, false);
                    } catch (DBException e) {
                    }
                    //response.addError(new String[]{finca, ex.getDBMessage()});

                }
            });
            if (db != null) {
                try {
                    db.commit();
                } catch (DBException ex1) {
                    Logger.getLogger(DatosECAAPPFacade.class.getName()).log(Level.SEVERE, null, ex1);
                } finally {
                    db.close();
                }

                db = null;
            }
        }
        return response;
    }

    public ResponseInterface procesarPuntos(ArrayList<PuntoGN> puntos) {
        if (puntos != null && !puntos.isEmpty()) {
            String sql = "UPDATE \"Clientes\" SET malla = ?, codord = ?, chipgn = ?, "
                    + "codinst = ? WHERE cliente = ?";
            puntos.stream().forEach((punto) -> {
                String poliza = punto.getPoliza();
                try {
                    if (db.updateQuery(sql, punto.getMalla(), punto.getCodOrd(),
                            punto.getChipGN(), punto.getCodInstalacion(), poliza) > 0) {
                        response.addOk(poliza);
                    } else {
                        response.addError(new String[]{poliza, "ERROR ACTUALIZANDO PUNTOS"});
                    }
                } catch (DBException ex) {
                    response.addError(new String[]{poliza, ex.getDBMessage()});
                } finally {
                    close();
                }
            });
        }
        return response;
    }

    public String finalizarProcesoCts() throws DBException {
        try {
            RowIndex sql = dbt.getRowIndex("SELECT COUNT(*) FROM \"CT_temp\" AS INDEX");
            RowIndex resultado = null;
            if (sql != null) {
                int cantidad = sql.getIntIndex();
                for (int i = 0; i < cantidad; i = i + 1000) {
                    resultado = dbt.getRowIndex("SELECT procesar_cts(" + i + ") AS index;");
                }

                if (resultado != null) {
                    return resultado.getStringIndex();
                }
            }
            return null;
        } finally {
            close();
        }
    }

    public String finalizarProcesoCtsTrafos() throws DBException {
        try {
            RowIndex sql = dbt.getRowIndex("SELECT COUNT(*) FROM \"CT_trafo_temp\" AS INDEX");
            RowIndex resultado = null;
            if (sql != null) {
                int cantidad = sql.getIntIndex();
                for (int i = 0; i < cantidad; i = i + 1000) {
                    resultado = dbt.getRowIndex("SELECT procesar_cts_trafo(" + i + ") AS index;");
                }

                if (resultado != null) {
                    return resultado.getStringIndex();
                }
            }
            return null;
        } finally {
            close();
        }
    }

    public String finalizarProcesoFincas() throws DBException {
        try {
            RowIndex sql = dbt.getRowIndex("SELECT COUNT(*) FROM \"Fincas_temp\" AS INDEX");
            RowIndex resultado = null;
            if (sql != null) {
                int cantidad = sql.getIntIndex();
                for (int i = 0; i < cantidad; i = i + 1000) {
                    resultado = dbt.getRowIndex("SELECT procesar_fincas(" + i + ") AS index;");
                }
                if (resultado != null) {
                    return resultado.getStringIndex();
                }
            }
            return null;
        } finally {
            close();
        }
    }
    
    public String finalizarProcesoCtsT() throws MyException, DBException {
        DBQuery dbq = new DBQuery(DB_POOL);
        try {
            RowIndex sql = dbq.getUnique("SELECT COUNT(*) FROM \"Fincas_temp\" AS INDEX", RowIndex.class);
            RowIndex resultado = null;
            if (sql != null) {
                int cantidad = sql.getIntIndex();
                resultado = dbq.getUnique("SELECT procesar_fincas() AS index;", RowIndex.class);
                if (resultado != null) {
                    return resultado.getStringIndex();
                }
            }
            return null;
        } finally {
            dbq.close();
        }
    }

    public String finalizarProcesoFincasT() throws MyException, DBException {
        DBQuery dbq = new DBQuery(DB_POOL);
        try {
            RowIndex sql = dbq.getUnique("SELECT COUNT(*) FROM \"Fincas_temp\" AS INDEX", RowIndex.class);
            RowIndex resultado = null;
            if (sql != null) {
                int cantidad = sql.getIntIndex();
                resultado = dbq.getUnique("SELECT procesar_fincas() AS index;", RowIndex.class);
                if (resultado != null) {
                    return resultado.getStringIndex();
                }
            }
            return null;
        } finally {
            dbq.close();
        }
    }

    public String finalizarProcesoLocalidades() throws DBException {
        try {
            RowIndex sql = dbt.getRowIndex("SELECT COUNT(*) FROM \"Localidades_temp\" AS INDEX");
            RowIndex resultado = null;
            if (sql != null) {
                int cantidad = sql.getIntIndex();
                for (int i = 0; i < cantidad; i = i + 1000) {
                    resultado = dbt.getRowIndex("SELECT procesar_localidades(" + i + ") AS index;");
                }
                if (resultado != null) {
                    return resultado.getStringIndex();
                }
            }
            return null;
        } finally {
            close();
        }
    }

    public String finalizarProcesoProvincias() throws DBException {
        try {
            RowIndex sql = dbt.getRowIndex("SELECT COUNT(*) FROM \"Provincias_temp\" AS INDEX");
            RowIndex resultado = null;
            if (sql != null) {
                int cantidad = sql.getIntIndex();
                for (int i = 0; i < cantidad; i = i + 1000) {
                    resultado = dbt.getRowIndex("SELECT procesar_provincias(" + i + ") AS index;");
                }
                if (resultado != null) {
                    return resultado.getStringIndex();
                }
            }
            return null;
        } finally {
            close();
        }
    }

    public String finalizarProcesoRedes() throws DBException {
        try {

            RowIndex sql = dbt.getRowIndex("SELECT COUNT(*) FROM \"RED-MT-BT_temp\" AS INDEX");
            RowIndex resultado = null;
            if (sql != null) {
                int cantidad = sql.getIntIndex();
                for (int i = 0; i < cantidad; i = i + 1000) {
                    resultado = dbt.getRowIndex("SELECT procesar_redes(" + i + ") AS index;");
                }
                if (resultado != null) {
                    return resultado.getStringIndex();
                }
            }
            return null;
        } finally {
            close();
        }
    }

    public String finalizarProcesoTrafos() throws DBException {
        try {
            RowIndex sql = dbt.getRowIndex("SELECT COUNT(*) FROM \"TrafoSuministros_temp\" AS INDEX");
            RowIndex resultado = null;
            if (sql != null) {
                int cantidad = sql.getIntIndex();
                for (int i = 0; i < cantidad; i = i + 1000) {
                    resultado = dbt.getRowIndex("SELECT procesar_trafos(" + i + ") AS index;");
                }
                if (resultado != null) {
                    return resultado.getStringIndex();
                }
            }
            return null;
        } finally {
            close();
        }
    }

}
