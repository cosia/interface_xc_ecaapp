package com.co.extreme.interfaces.ws.classes;

/**
 *
 * @author Cristian Z. Osia
 */
public class CtsTrafoECAAPP {
    
    private String codTrafo;
    private String matTrafo;
    private String codCt;
    private String matCt;
    private String potNominal;
    private String codSalmt;
    private String matSalmt;
    private String nombreSalmt;
    private String nroCentro;
    private String empresa;
    private String nroCmd;
    private String zona;
    private String nroMesa;
    private String delegacion;
    private String propiedad;
    private String localizacion;
    private String latitud;
    private String longitud;
    private String theGeom;

    public String getCodTrafo() {
        return codTrafo;
    }

    public void setCodTrafo(String codTrafo) {
        this.codTrafo = codTrafo;
    }

    public String getMatTrafo() {
        return matTrafo;
    }

    public void setMatTrafo(String matTrafo) {
        this.matTrafo = matTrafo;
    }

    public String getCodCt() {
        return codCt;
    }

    public void setCodCt(String codCt) {
        this.codCt = codCt;
    }

    public String getMatCt() {
        return matCt;
    }

    public void setMatCt(String matCt) {
        this.matCt = matCt;
    }

    public String getPotNominal() {
        return potNominal;
    }

    public void setPotNominal(String potNominal) {
        this.potNominal = potNominal;
    }

    public String getCodSalmt() {
        return codSalmt;
    }

    public void setCodSalmt(String codSalmt) {
        this.codSalmt = codSalmt;
    }

    public String getMatSalmt() {
        return matSalmt;
    }

    public void setMatSalmt(String matSalmt) {
        this.matSalmt = matSalmt;
    }

    public String getNombreSalmt() {
        return nombreSalmt;
    }

    public void setNombreSalmt(String nombreSalmt) {
        this.nombreSalmt = nombreSalmt;
    }

    public String getNroCentro() {
        return nroCentro;
    }

    public void setNroCentro(String nroCentro) {
        this.nroCentro = nroCentro;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getNroCmd() {
        return nroCmd;
    }

    public void setNroCmd(String nroCmd) {
        this.nroCmd = nroCmd;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getNroMesa() {
        return nroMesa;
    }

    public void setNroMesa(String nroMesa) {
        this.nroMesa = nroMesa;
    }

    public String getDelegacion() {
        return delegacion;
    }

    public void setDelegacion(String delegacion) {
        this.delegacion = delegacion;
    }

    public String getPropiedad() {
        return propiedad;
    }

    public void setPropiedad(String propiedad) {
        this.propiedad = propiedad;
    }

    public String getLocalizacion() {
        return localizacion;
    }

    public void setLocalizacion(String localizacion) {
        this.localizacion = localizacion;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getTheGeom() {
        return theGeom;
    }

    public void setTheGeom(String theGeom) {
        this.theGeom = theGeom;
    }
    
}
