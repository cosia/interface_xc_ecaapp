package com.co.extreme.interfaces.ws.rest;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 *
 * @author Cristian Z. Osia
 */
@ApplicationPath("ws")
public class ApplicationConfig extends Application {
    
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> myClass = new HashSet();
        myClass.add(ECAAPPRest.class);
        return myClass;
    }
    
}
