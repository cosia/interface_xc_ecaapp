package com.co.extreme.interfaces.ws.classes;

/**
 *
 * @author Cristian Z. Osia
 */
public class RedECAAPP {
    
    private String zonaSub;
    private String corSub;
    private String sectorSub;
    private String codSub;
    private String nomSub;
    private String codCircuito;
    private String nomCircuito;
    private String codCt;
    private String nomCt;
    private String placaBlanca;
    private String codTrafo;
    private String nomTrafo;
    private String placaAmarilla;

    public String getZonaSub() {
        return zonaSub;
    }

    public void setZonaSub(String zonaSub) {
        this.zonaSub = zonaSub;
    }

    public String getCorSub() {
        return corSub;
    }

    public void setCorSub(String corSub) {
        this.corSub = corSub;
    }

    public String getSectorSub() {
        return sectorSub;
    }

    public void setSectorSub(String sectorSub) {
        this.sectorSub = sectorSub;
    }

    public String getCodSub() {
        return codSub;
    }

    public void setCodSub(String codSub) {
        this.codSub = codSub;
    }

    public String getNomSub() {
        return nomSub;
    }

    public void setNomSub(String nomSub) {
        this.nomSub = nomSub;
    }

    public String getCodCircuito() {
        return codCircuito;
    }

    public void setCodCircuito(String codCircuito) {
        this.codCircuito = codCircuito;
    }

    public String getNomCircuito() {
        return nomCircuito;
    }

    public void setNomCircuito(String nomCircuito) {
        this.nomCircuito = nomCircuito;
    }

    public String getCodCt() {
        return codCt;
    }

    public void setCodCt(String codCt) {
        this.codCt = codCt;
    }

    public String getNomCt() {
        return nomCt;
    }

    public void setNomCt(String nomCt) {
        this.nomCt = nomCt;
    }

    public String getPlacaBlanca() {
        return placaBlanca;
    }

    public void setPlacaBlanca(String placaBlanca) {
        this.placaBlanca = placaBlanca;
    }

    public String getCodTrafo() {
        return codTrafo;
    }

    public void setCodTrafo(String codTrafo) {
        this.codTrafo = codTrafo;
    }

    public String getNomTrafo() {
        return nomTrafo;
    }

    public void setNomTrafo(String nomTrafo) {
        this.nomTrafo = nomTrafo;
    }

    public String getPlacaAmarilla() {
        return placaAmarilla;
    }

    public void setPlacaAmarilla(String placaAmarilla) {
        this.placaAmarilla = placaAmarilla;
    }
    
}
