package com.co.extreme.interfaces.ws.classes;

/**
 *
 * @author Cristian Z. Osia
 */
public class PolizaGN {
    
    private String categoria;
    private String clienteTipo;
    private String clienteSituacion;
    private String direccion;
    private String estrato;
    private String fechaContratacion;
    private String fechaPuestaServicio;
    private String medidor;
    private String medidorMarca;
    private String medidorTipo;
    private String municipio;
    private String nombre;
    private String poliza;
    private String tarifa;
    private String telefono;
    private String latitud;
    private String longitud;
    private String malla;
    private String tipoContador;

    public PolizaGN() {
    }

    public String getCategoria() {
        return categoria;
    }

    public String getClienteTipo() {
        return clienteTipo;
    }

    public String getClienteSituacion() {
        return clienteSituacion;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getEstrato() {
        return estrato;
    }

    public String getFechaContratacion() {
        return fechaContratacion;
    }

    public String getFechaPuestaServicio() {
        return fechaPuestaServicio;
    }

    public String getMedidor() {
        return medidor;
    }

    public String getMedidorMarca() {
        return medidorMarca;
    }

    public String getMedidorTipo() {
        return medidorTipo;
    }

    public String getMunicipio() {
        return municipio;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPoliza() {
        return poliza;
    }

    public String getTarifa() {
        return tarifa;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getMalla() {
        return malla;
    }

    public void setMalla(String malla) {
        this.malla = malla;
    }

    public String getTipoContador() {
        return tipoContador;
    }

    public void setTipoContador(String tipoContador) {
        this.tipoContador = tipoContador;
    }
    
}
